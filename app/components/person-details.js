import Ember from 'ember';

/**
 * Person details component, used for the sidebar contents on "our people"
 */
export default Ember.Component.extend({
  tagName: 'div',
  person: null,
});
