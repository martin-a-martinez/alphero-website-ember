import Ember from 'ember';

/**
 * Summary for case studies, displays the "squares" on the case studies page
 */
export default Ember.Component.extend({
  tagName: 'div',
  caseStudy: null,
});
