import Ember from 'ember';

/**
 * Client logo component
 */
export default Ember.Component.extend({
  tagName: 'li',
  classNames: 'Sector-logoList-item',
  logo: null,
  name: '',
});
