import Ember from 'ember';

/**
 * Details containing the now hiring text, which is reused on the "contact" page
 * as well as on the "our people" page.
 */
export default Ember.Component.extend({
  tagName: 'div',
});
