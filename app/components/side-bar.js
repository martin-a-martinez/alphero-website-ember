import Ember from 'ember';

/**
 * Expandable side bar, in use on the "our people" page (and possibly elsewhere?)
 */
export default Ember.Component.extend({
  tagName: 'div',

  //All optional
  title: null,
  summary: null,
  image: null,

  //Close action is to be passed to the component, so that the parent template
  //or component can control the behaviour of opening/closing the sidebar
  close: null,
  actions: {
    close() {
      this.close();
    },
  },
});
