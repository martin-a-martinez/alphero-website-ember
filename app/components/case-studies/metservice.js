import Ember from 'ember';

/**
 * Custom case study layout
 */
export default Ember.Component.extend({
  tagName: 'div',
  classNames: ['CaseStudy--Metservice'], //NOTE: Can specify custom classes
  caseStudy: null,
});
