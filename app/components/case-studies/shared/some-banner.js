import Ember from 'ember';

/**
 * Example shared component for case study pages
 */
export default Ember.Component.extend({
  tagName: 'div',
  title: null,
  text: null,
});
