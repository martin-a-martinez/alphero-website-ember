import Ember from 'ember';

/**
 * Generic case study layout
 */
export default Ember.Component.extend({
  tagName: 'div',
  caseStudy: null,
});
