import Ember from 'ember';

/**
 * Sector summary for the "sectors" page
 */
export default Ember.Component.extend({
  tagName: 'div',
  sector: null,
});
