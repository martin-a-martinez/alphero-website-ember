import Ember from 'ember';

/**
 * Global site wide header
 */
export default Ember.Component.extend({
  tagName: 'header',
  classNames: 'Header',
});
