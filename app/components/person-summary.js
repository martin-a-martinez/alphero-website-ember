import Ember from 'ember';

/**
 * Person summary used in the "our people" page
 */
export default Ember.Component.extend({
  tagName: 'li',
  classNames: 'PersonList-person',
  name: null,
  title: null,
  photo: null,
});
