import Ember from 'ember';

const {
  computed,
  String: {
    htmlSafe
  },
} = Ember;

export default Ember.Component.extend({
  tagName: 'footer',
  classNames: 'Footer',

  copyright: computed(function () {
    let currentYear = new Date().getFullYear();
    return htmlSafe(`Copyright &copy; ${currentYear} Alphero. All rights reserved.`);
  }),

});
