import Ember from 'ember';

const {
  computed: {
    notEmpty,
  }
} = Ember;

export default Ember.Object.extend({
  hasAward: notEmpty('award'),
  hasComponent: notEmpty('component'),
});
