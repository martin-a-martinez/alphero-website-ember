import Ember from 'ember';
import computed from 'ember-macro-helpers/computed';

export default Ember.Object.extend({
  hasSocials: computed('social', (social) => {
    return (social && Object.keys(social).length > 0);
  }),
});
