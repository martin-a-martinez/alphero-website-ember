import Ember from 'ember';
import caseStudies from 'alphero-website/data/case-studies';
import categories from 'alphero-website/data/project-categories';
import sectors from 'alphero-website/data/sectors';

export default Ember.Route.extend({
  model() {
    return {
      sectors,
      categories,
      caseStudies: caseStudies
        .filter(caseStudy => caseStudy.isFeatured)
        .filter(caseStudy => !caseStudy.isHidden),
    };
  },
});
