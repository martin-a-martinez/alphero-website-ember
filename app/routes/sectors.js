import Ember from 'ember';
import Sector from 'alphero-website/models/sector';
import sectors from 'alphero-website/data/sectors';

export default Ember.Route.extend({
  model() {
    return {
      sectors: sectors
        .map(sector => Sector.create(sector)),
    };
  },
});
