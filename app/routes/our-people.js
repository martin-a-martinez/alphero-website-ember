import Ember from 'ember';
import Person from 'alphero-website/models/person';
import Category from 'alphero-website/models/category';
import people from 'alphero-website/data/people';
import categories from 'alphero-website/data/people-categories';
import awards from 'alphero-website/data/awards';

export default Ember.Route.extend({
  model() {
    return {
      awards,
      categories: categories
        .map(category => Category.create(category)),
      people: people
        .filter(person => !person.isArchived)
        .map(person => Person.create(person)),
    };
  },
});
