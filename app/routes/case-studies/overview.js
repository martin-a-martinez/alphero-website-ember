import Ember from 'ember';
import CaseStudy from 'alphero-website/models/case-study';
import Category from 'alphero-website/models/category';
import Sector from 'alphero-website/models/sector';
import Client from 'alphero-website/models/client';
import caseStudies from 'alphero-website/data/case-studies';
import categories from 'alphero-website/data/project-categories';
import sectors from 'alphero-website/data/sectors';
import clients from 'alphero-website/data/clients';

export default Ember.Route.extend({
  model() {
    return {
      sectors: sectors
        .map(sector => Sector.create(sector)),
      clients: clients
        .map(client => Client.create(client)),
      categories: categories
        .map(category => Category.create(category)),
      caseStudies: caseStudies
        .filter(caseStudy => !caseStudy.isHidden)
        .map(caseStudy => CaseStudy.create(caseStudy)),
    };
  },
});
