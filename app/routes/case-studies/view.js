import Ember from 'ember';
import CaseStudy from 'alphero-website/models/case-study';
import caseStudies from 'alphero-website/data/case-studies';

export default Ember.Route.extend({
  model(params) {

    //Find non-hidden case study with a route
    const caseStudy = caseStudies
      .filter(caseStudy => !caseStudy.isHidden)
      .filter(caseStudy => caseStudy.hasRoute)
      .find(caseStudy => caseStudy.id === params.caseStudyId);

    //Convert to model if found
    return {
      caseStudy: caseStudy ? CaseStudy.create(caseStudy) : null,
    };
  },
  afterModel(model) {
    this._super(...arguments);
    if (!model.caseStudy) {
      this.transitionTo('caseStudies.overview');
    }
  }
});
