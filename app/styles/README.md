# AINT CSS

  ## About AINT CSS

  **A**lphero **IN**verted **T**riangle CSS framework builds upon ideas and methodologies found in the following CSS frameworks: [suitcss](http://suitcss.github.io/), [inuitcss](https://github.com/inuitcss/getting-started) and [itcss](https://github.com/gpmd/itcss-boilerplate).

  It is designed for component-based approaches to UI development with a collection of CSS packages and build tools available as modules.

  The ‘inverted triangle’ aspect of AINT comes from the order in which the SCSS files are loaded and consequently collated. The premise being that *styles are layered up in order of specificity* starting with the least specific.


## SCSS File Structure

```html
├── app.scss
├── base
│   ├── elements
│   │   ├── _index.scss
│   │   ├── icons.scss
│   │   ├── naturalize.scss
│   │   └── typography.scss
│   ├── mixins
│   │   ├── _index.scss
│   │   ├── aspect-ratio.scss
│   │   ├── borders.scss
│   │   ├── dl-columns.scss
│   │   ├── easing.scss
│   │   ├── fit-media.scss
│   │   ├── font-size.scss
│   │   ├── hover-active-focus.scss
│   │   ├── media-queries.scss
│   │   ├── px-to-rem.scss
│   │   ├── retina-images.scss
│   │   └── theme.scss
│   └── settings
│       ├── _index.scss
│       ├── fonts.scss
│       └── variables.scss
├── components
│   ├── _index.scss
│   ├── alert.scss
│   ├── button.scss
│   ├── calendar.scss
│   ├── forms
│   │   ├── _index.scss
│   │   ├── checkbox.scss
│   │   ├── input.scss
│   │   ├── radio.scss
│   │   ├── select.scss
│   │   ├── textarea.scss
│   │   └── variables.scss
│   ├── hamburger.scss
│   ├── hint.scss
│   ├── link.scss
│   ├── media.scss
│   ├── modal.scss
│   ├── overlay.scss
│   ├── pagination.scss
│   ├── spinner.scss
│   ├── table.scss
│   ├── tile.scss
│   ├── token.scss
│   └── tooltips.scss
├── layout
│   ├── _index.scss
│   ├── bleed.scss
│   ├── container.scss
│   ├── grid.scss
│   ├── gutter.scss
│   └── layout.scss
├── print.scss
├── utilities
│   ├── _index.scss
│   ├── display.scss
│   ├── flex.scss
│   ├── link.scss
│   ├── margin.scss
│   ├── offset-after.scss
│   ├── offset-before.scss
│   ├── padding.scss
│   ├── responsive.scss
│   ├── text.scss
│   └── width.scss
└── wip.scss
```

## Import order

```css
// - start with nothing and layer up our specificity
@import "base/settings/index"; // \•••••••••••••/ global variables and fonts
@import "base/mixins/index";   //  \•••••••••••/  mixins and functions
@import "base/elements/index"; //   \•••••••••/   html elements (type selectors) and naturalize
@import "layout/index";        //    \•••••••/    layout and layout helpers, e.g. .Grid, .Container
@import "components/index";    //     \•••••/     designed components / chunks of ui, layout helpers
@import "wip";                 //      \•••/      working file or one-off styles (too small for components)
@import "utilities/index";     //       \•/       helpers and overrides (trumps)
@import "print";               //        •
```

## Code conventions

*Please familiarise yourself with, and adhere to, the adopted canventions as outlined in this document.*

### HTML syntax
* Use two-space indentations
* use double quotes on attributes
* don’t include a trailing slash in self-closing elements, e.g. do this: `<img …>`, but not this: `<img … />`
* a blank line (return) and the end of each file.

### Image names

* All lowercase, with each word hyphen separated, never use underscores.

### SCSS syntax

* Use two-space indentations
* declarations should appear on their own line
* lowercase and shorthand hex values, e.g. `#fff`
* don’t specify units for zero values, e.g., `margin: 0;` instead of `margin: 0px;`.
* include one space before opening curlies `{`
* include one space after colon `:` for each declaration
* a blank line between rulesets
* only `@extend %` placeholder classes, i.e. not dot (`@extend .`)
* don't use inline utility classes in the HTML, e.g. `<div class="u-floatRight">...`, with the exception of layout utilities such as `.u-width1of...` or `.Grid`
* a blank line (return) and the end of each file.

### [BEM](http://google.com/?search=BEM%20naming%convention) (Block Element Modifier) naming conventions

#### Components

Syntax: `[<namespace>-]<ComponentName>[-descendentName][--modifierName]`

```css
// Block (Component)
.MyComponent {}

// Modifier
.MyComponent--modifier {}

// Element
.MyComponent-part {}
.MyComponent-anotherPart {}

// State – note: append state classnames to the associated element selector
.MyComponent.is-active {}
```
**Important:** please use the same upper/lower case and hyphenation conventions as outlined here. Never use underscores.

#### Utilities

Syntax: `u-[sm-|md-|lg-]<utilityName>`

```html
<div class="u-md-width1of2 u-lg-width1of4">
	…
</div>
```

**Note:** This is "mobile first", i.e. the above defaults to sm- (in this case 100% wide), then md- and lg- are declared inside media queries in the css.

**Warning:** Because we are sharing styles and markup across multiple sites, do not attach inline *styling* (as opposed to layout) utility classes such as `class="u-textAlignLeft"` in the HTML markup. Rather @extend the utility in the site-specific component, e.g.

```css
.MyComponent {
  @extend %u-textAlignLeft;
}
```

### Some examples and tips

#### Tip 1.

Stick to BEM.  
Nope:

```html
<div class="ChartList-item">
  <button class="ChartList-select">Select</button>
  <div class="ChartList-artwork">
    <img src="...">
  </div>
  <div class="mw-chart-song-info">
    <div class="mw-chart-name">...</div>
    <div class="mw-chart-artist">...</div>
  </div>
</div>
```

Yep:

```html
<div class="ChartList">
  <div class="ChartList-item">
    <button class="ChartList-item-select">Select</button>
    <div class="ChartList-item-artwork">
      <img src="...">
    </div>
    <div class="ChartList-item-songInfo">
      <div class="ChartList-item-songInfo-name">...</div>
      <div class="ChartList-item-songInfo-artist">...</div>
    </div>
  </div>
  <div class="ChartList-item">
    …
  <div>
</div>
```

We can now write easy-to-read, un-nested (See Tip 2.) scss.  
Nope:

```css
.ChartList {
  .mw-chart-song-info {
    margin: 0 20px 0 0;
    width: calc(100% - 170px);

    .mw-chart-name {
      font-size: 16px;
      font-weight: bold;
      margin: 0 0 10px;
    }

    .mw-chart-artist {
      font-size: 14px;
      line-height: 1.5;
    }
  }
}
```

Yep:

```css
$chartlist-song-info-width: 170px;

.ChartList {
  ...
}

.ChartList-item {
  ...
}

.ChartList-item-songInfo {
  margin-right: $h;
  width: calc(100% - #{$chartlist-song-info-width});
}

.ChartList-item-songInfo-name {
  @include font-size (16px);
  font-family: $font-default-bold;
  margin-bottom: $v/2;
}

.ChartList-item-songInfo-artist {
  @include font-size (14px, 21px);
}
```

Note the use of variables, our mixins and spacing units ($v and $h) above to help enforce consistency.

#### Tip 2.

As you can see from the above, BEM lends itself to flat CSS. It helps us to avoid unnecessarily lengthy selectors. **Beware the dreaded echelon of gulls!**

```css
            ...
          }
        }
      }
    }
  }
}
```

#### Tip 3.

Don't specify values unless you need to, e.g. resist the temptation to take this shortcut:

```css
.blah {
  margin: 0 auto;
}
```

when what you actually mean is:

```css
.blah {
  margin-left: auto;
  margin-right: auto;
}
```

i.e. being specific is not only clearer, but saves other developers time.

#### Tip 4.

Don't use font-weight: bold (or italic) when we have a font family that has a bold variant included, as the browser will employ faux bolding on top of the already bold font.

### Further Reading / Useful Links

* Development Browser Extensions
  * [CSS Dig Chrome extension](https://chrome.google.com/webstore/detail/css-dig/lpnhmlhomomelfkcjnkcacofhmggjmco):  _Analyze your CSS in a new way. Consolidate, refactor, and gawk at the 37 shades of blue your site somehow ended up with._
  * [HTML Code Sniffer Bookmarklet](http://squizlabs.github.io/HTML_CodeSniffer/): _A client-side script that checks HTML source code and detects violations of a defined coding standard._
