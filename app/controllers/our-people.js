import Ember from 'ember';
import computed from 'ember-macro-helpers/computed';

const {
  set,
  computed: {
    reads,
  },
} = Ember;

export default Ember.Controller.extend({
  //Awards section
  awards: reads('model.awards'),

  //Category filter
  category: null,

  //Active person (shown in sidebar)
  activePerson: null,

  //Toggles the "now hiring" sidebar
  showHiring: false,

  //Categories and people filtered on category
  categories: reads('model.categories'),
  people: computed('model.people', 'category', (people, category) => {
    if (!category) {
      return people;
    }
    return people
      .filter(person => person.categories.some(cat => cat === category));
  }),

  //Controls for active category/person
  actions: {
    setCategory(category) {
      set(this, 'category', category);
    },
    setActivePerson(person) {
      set(this, 'activePerson', person);
      set(this, 'showHiring', false);
    },
    toggleNowHiringDetails() {
      this.toggleProperty('showHiring');
      set(this, 'activePerson', null);
    },
  },
});
