import Ember from 'ember';
import computed from 'ember-macro-helpers/computed';

const {
  set,
  computed: {
    reads,
  },
} = Ember;

export default Ember.Controller.extend({

  //Controls the category filter, and allows us to link to this page with
  //a category filter pre-setup
  queryParams: ['category'],
  category: null,

  //Clients, sectors and categories
  clients: reads('model.clients'),
  sectors: reads('model.sectors'),
  categories: reads('model.categories'),

  //Case studies filtered on category
  caseStudies: computed('model.caseStudies', 'category', (caseStudies, category) => {
    if (!category) {
      return caseStudies;
    }
    return caseStudies
      .filter(caseStudy => caseStudy.categories.some(cat => cat === category));
  }),

  //Actions
  actions: {
    setCategory(category) {
      set(this, 'category', category);
    },
  },
});
