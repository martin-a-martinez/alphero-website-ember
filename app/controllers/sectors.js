import Ember from 'ember';

const {
  computed: {
    reads,
  },
} = Ember;

export default Ember.Controller.extend({
  sectors: reads('model.sectors'),
});
