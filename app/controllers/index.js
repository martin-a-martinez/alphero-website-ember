import Ember from 'ember';
import computed from 'ember-macro-helpers/computed';

const {
  computed: {
    reads,
  },
} = Ember;

export default Ember.Controller.extend({
  queryParams: ['category'],
  category: null,

  //Sectors and categories
  sectors: reads('model.sectors'),
  categories: reads('model.categories'),

  //Case studies filtered on category
  caseStudies: computed('model.caseStudies', 'category', (caseStudies, category) => {
    if (!category) {
      return caseStudies;
    }
    return caseStudies
      .filter(caseStudy => caseStudy.categories.some(cat => cat === category));
  }),
});
