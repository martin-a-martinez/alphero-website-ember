import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('index', { path: '/' });
  this.route('what-we-do');
  this.route('our-people');
  this.route('sectors');
  this.route('contact');
  this.route('thank-you');
  this.route('case-studies', function() {
    this.route('overview', {
      path: '/',
    });
    this.route('view', {
      path: '/:caseStudyId',
    });
  });
});

export default Router;
