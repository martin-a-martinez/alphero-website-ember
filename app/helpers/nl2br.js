import Ember from 'ember';

const {
  Helper,
  String: {
    htmlSafe,
  },
} = Ember;

export default Helper.extend({
  compute([text]) {
    if (typeof text === 'number') {
      return String(text);
    }
    if (typeof text !== 'string' || (text = String(text)) === '') {
      return '';
    }
    return htmlSafe(
      text.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1<br>')
    );
  },
});
