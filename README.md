# alphero-website

## Installation

* `yarn`

## Running / Development

* `yarn start`
* Visit [http://localhost:4200](http://localhost:4200).

## Building

* `yarn build` (development)
* `yarn build:prod` (production)

## Deploying

To deploy to Netlify, run a build for production using `yarn build:prod` and login to [Netlify](https://app.netlify.com/sites/alphero-website/deploys) with the apps@alphero.com account. Scroll to the bottom of the deploys page and drag the `dist` folder onto the target area to trigger a manual deployment. This should take a few seconds and once done the new deployment will be live. Netlify's CDN will automatically purge any stale assets after a new deployment so there should be no caching issues.

You can disable Netlify's auto-publish feature to deploy a version and preview it on Netlify's test URL before publishing it live.

The Netlify preview URL is at https://alphero-website.netlify.com

## Things TODO before going live

1. Make the contact form work via ajax (see `contact.hbs` and comments there)
2. Check if we can process/upload files via the Netlify form handling as well (for CV)
  a. If yes, all good
  b. If not, need to think up another strategy for this. Possibly a 3rd party form handler, or upload the CV's to S3?
3. Figure out best structure for case study pages. Currently, a `component` can be specified in the data of a case study, and this can point to a case study specific component with a layout structure specific for that case study. (See for example `test-case-study.hbs`). These case study components can reference and use other, more generic components, like a case study header or content block. A `-generic.hbs` also exists for case studies that don't have a specific case study component.
4. Some kind of loading screen, for when the app is loading initially. Probably don't need a loading screen for in between transitions as these are pretty fast.
5. Implement all relevant jQuery plugins like Swiper etc.

## Things TODO for going live

1. Finish building the website and test it, making sure all pages, case studies etc. work as expected.
2. Deploy the latest version to Netlify
3. Setup the form handling to have the desired outgoing notifications (at https://app.netlify.com/sites/alphero-website/settings/forms). You can setup a simple email notification, or Slack integration.
4. Hook up alphero.com as a custom domain to the Netlify app (see https://www.netlify.com/docs/custom-domains/)
5. Update the DNS settings of alphero.com as needed to point to the Netlify URL.
6. Enable HTTPS by provisioning an SSL certificate via Netlify. This is fully automated and should only take a minute to be live.
7. Enable force HTTPS to ensure the site is always served over HTTPS.
8. Wait for DNS to update and verify new site is shown on alphero.com
9. Decommission the existing App Engine hosted solution to get rid of that $900 per year bill
10. Rejoice
